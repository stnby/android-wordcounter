package pl.stnby.wordcounter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView textInput = (TextView) findViewById(R.id.textInput);
        final TextView textOutput = (TextView) findViewById(R.id.textOutput);
        Button buttonCount = (Button) findViewById(R.id.button);

        buttonCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textOutput.setText("");
                if (textInput.getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), getString(R.string.warn_input_empty), Toast.LENGTH_SHORT).show();
                }
                else {
                    final long mode = ((Spinner)findViewById(R.id.selectMode)).getSelectedItemId();
                    if (mode == 0) {
                        textOutput.setText(String.valueOf(WordCounter.countWords(textInput.getText().toString())));
                    }
                    else {
                        textOutput.setText(String.valueOf(WordCounter.countPunctuation(textInput.getText().toString())));
                    }
                }
            }
        });

    }
}