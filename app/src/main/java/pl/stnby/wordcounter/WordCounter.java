package pl.stnby.wordcounter;

public class WordCounter {
    public static int countWords(String str) {
        return str.split("\\s+").length;
    }
    public static int countPunctuation(String str) {
        return str.split("\\w+").length;
    }
}